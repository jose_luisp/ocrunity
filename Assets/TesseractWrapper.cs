﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesseractWrapper : MonoBehaviour {
	
	#if UNITY_IOS
	private TesseractWrapper_iOS tesseractClass = new TesseractWrapper_iOS ();
	#elif UNITY_ANDROID
	private TesseractWrapper_And tesseractClass = new TesseractWrapper_And ();
	#endif

	public string Version()
	{
		return tesseractClass.Version ();

	}

	public string RecognizeFromFile(string filename)
	{
	
		int procCount = 0; 
		return tesseractClass.RecognizeFromFile (filename, procCount);

	}
		

	public string Recognize(Color32[] colors, int width, int height)
	{

		return tesseractClass.Recognize(colors, width, height);

	}

	public string RecognizeFromTexture(Texture2D texture, bool doCopy)
	{
		return tesseractClass.RecognizeFromTexture (texture, doCopy);

	}

	public bool Init(string lang, string dataPath)
	{
		return tesseractClass.Init(lang, dataPath);

	}

	public void Close()
	{
		tesseractClass.Close ();


	}
}
