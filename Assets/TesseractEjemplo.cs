﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class TesseractEjemplo : MonoBehaviour {
	public Texture2D texture1;
    public Texture2D texture2;

	public Text text1;
    public Text text2;

	void Start () {
		CopyFile("tessdata/", "eng.cube.bigrams");
		CopyFile("tessdata/", "eng.cube.fold");
		CopyFile("tessdata/", "eng.cube.lm");
		CopyFile("tessdata/", "eng.cube.nn");
		CopyFile("tessdata/", "eng.cube.params");
		CopyFile("tessdata/", "eng.cube.size");
		CopyFile("tessdata/", "eng.cube.word-freq");
		CopyFile("tessdata/", "eng.tesseract_cube.nn");
		CopyFile("tessdata/", "eng.traineddata");
		CopyFile("tessdata/", "eng.user-patterns");
		CopyFile("tessdata/", "eng.user-words");
		CopyFile("tessdata/", "osd.traineddata");
        CopyFile("tessdata/", "pdf.ttf");
		CopyFile("tessdata/tessconfigs/", "debugConfigs.txt");
		CopyFile("tessdata/tessconfigs/", "recognitionConfigs.txt");
        CopyFile("tessdata/", "spa.traineddata");
	
		TesseractWrapper_And tesseract1 = new TesseractWrapper_And();
        TesseractWrapper_And tesseract2 = new TesseractWrapper_And();

        string datapath = System.IO.Path.Combine(Application.persistentDataPath, "tessdata");
		tesseract1.Init("eng", datapath);
        tesseract2.Init("spa", datapath);

        string result1 = tesseract1.RecognizeFromTexture(texture1, false);
        string result2 = tesseract2.RecognizeFromTexture(texture2, false);

        text1.text = result1 ?? "Error: " + tesseract1.errorMsg;
        text2.text = result2 ?? "Error: " + tesseract2.errorMsg;

    }

	void CopyFile(string folder, string file) {
		string fileUrl = System.IO.Path.Combine(Application.streamingAssetsPath, folder + file);
		string fileDirectory = System.IO.Path.Combine(Application.persistentDataPath, folder);
		string filePath = System.IO.Path.Combine(fileDirectory, file);
		Debug.Log("Copying: " + fileUrl);
		if(!Directory.Exists(fileDirectory)) {
			Directory.CreateDirectory(fileDirectory);
		}
		WWW www = new WWW(fileUrl);
		while(!www.isDone){
		}
		File.WriteAllBytes(filePath, www.bytes);
		Debug.Log("file copy done ("+www.bytes.Length.ToString()+"): " + filePath);
		www.Dispose ();
		www = null;
	}
}
